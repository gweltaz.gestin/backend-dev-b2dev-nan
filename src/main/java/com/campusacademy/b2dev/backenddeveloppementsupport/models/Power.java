package com.campusacademy.b2dev.backenddeveloppementsupport.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Data // => @ToString, @EqualsAndHashCode, @Getter, @Setter, @RequiredArgsConstructor
@Entity
@Table(name = "power")
public class Power {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "powers", fetch = FetchType.LAZY)
    private List<SuperHero> superHeros;
}
