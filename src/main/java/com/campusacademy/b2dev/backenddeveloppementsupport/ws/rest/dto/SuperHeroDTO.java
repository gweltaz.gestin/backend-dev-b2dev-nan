package com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SuperHeroDTO {
    private Long id;
    private String superHeroName;
    private String secretIdentity;
    private List<PowerDTO> powers;
}
