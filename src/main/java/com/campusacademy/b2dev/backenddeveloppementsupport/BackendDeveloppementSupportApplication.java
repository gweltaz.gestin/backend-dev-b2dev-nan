package com.campusacademy.b2dev.backenddeveloppementsupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendDeveloppementSupportApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendDeveloppementSupportApplication.class, args);
    }
}
