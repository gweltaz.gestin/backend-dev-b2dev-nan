package com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest;

import com.campusacademy.b2dev.backenddeveloppementsupport.repositories.SuperHeroRepository;
import com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest.dto.PowerDTO;
import com.campusacademy.b2dev.backenddeveloppementsupport.ws.rest.dto.SuperHeroDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "superHeroes")
@RequiredArgsConstructor
public class SuperHeroesController {

    private final SuperHeroRepository superHeroRepository;
    // private final PowerMapper powerMapper;

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAllSuperHeroes() {
        return ResponseEntity.ok(this.superHeroRepository.findAll()
                .stream()
                // .map(superHero -> new SuperHeroDTO(superHero.getId(), superHero.getSuperHeroName(), superHero.getSecretIdentity()))
                // .map(superHero -> {
                //     return new SuperHeroDTO(superHero.getId(), superHero.getSuperHeroName(), superHero.getSecretIdentity())
                // })
                .map(superHero -> {
                    List<PowerDTO> powers;

                    powers = superHero.getPowers()
                            .stream()
                            // .map(power -> powerMapper.map(power))
                            .map(power -> new PowerDTO(power.getId(), power.getName(), power.getDescription()))
                            .collect(Collectors.toList());

                    return new SuperHeroDTO(superHero.getId(), superHero.getSuperHeroName(), superHero.getSecretIdentity(), powers);
                })
                .collect(Collectors.toList()));
    }
}
// [1,2,3,4]
// 1 => "1"
// 2 => "2"
// 3 => "3"
// 4 => "4"
// ["1","2","3","4"]
