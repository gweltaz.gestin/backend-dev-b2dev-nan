package com.campusacademy.b2dev.backenddeveloppementsupport.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Table(name = "superhero")
public class SuperHero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(
            name = "superhero_name",
            nullable = false,
            length = 100
    )
    private String superHeroName;

    private String secretIdentity;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name="power_superheros",
            joinColumns = { @JoinColumn(name = "superheros_id") },
            inverseJoinColumns = { @JoinColumn(name = "power_id") }
    )
    private List<Power> powers;

    @OneToOne(cascade = CascadeType.PERSIST)
    private Vilain nemesis;

    @ManyToOne
    @JoinColumn(name = "mentor_id")
    private SuperHero mentor;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "mentor")
    private List<SuperHero> sidekicks;
}

