package com.campusacademy.b2dev.backenddeveloppementsupport.repositories;

import com.campusacademy.b2dev.backenddeveloppementsupport.models.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {

    // SELECT * FROM superhero WHERE superhero_name = :superHeroName ORDER BY superhero_name limit 1 => on attend 1 superhero
    SuperHero getFirstBySuperHeroNameOrderBySuperHeroName(String superHeroName);

    // SELECT * FROM superhero WHERE superhero_name = :superHeroName => on attend * superhero
    List<SuperHero> findAllBySuperHeroName(String superHeroName);
}
